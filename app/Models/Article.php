<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Article extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'source',
        'title',
        'description',
        'publishedAt',
        'category_id','url'
    ];

//    protected $casts = [
//        'source' => 'json',
//    ];

     public function category(){
         return $this->belongsTo('App\Models\Category','category_id');
     }
}

<?php
namespace App\Http\Middleware;

use Closure;
use Config;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Route;

class AdminPermission
{

    public function __construct(Guard $auth)
    {

        $this->auth = $auth;

    }
    public function handle($request, Closure $next)
    {
        if ($request->ajax()) {

            return response('Unauthorized.', 401);
        } elseif(!str_contains('api',Route::currentRouteName())) {

                return redirect('/admin/login');
        }

    }

}

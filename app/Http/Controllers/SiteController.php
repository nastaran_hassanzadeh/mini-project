<?php
namespace App\Http\Controllers;

use App\Http\Controllers;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;

class SiteController extends Controller
{
     public function index(){
         $categories = array();
         $categories[0] = 'All Categories';
         $category = Category::get();

         foreach ($category as $key => $value) {
             $categories[$value->id] = $value->title;
         }
         return view('welcome',compact('categories'));
     }

     public function filter(Request $request)
     {
        $query = Article::query();
        if($request->get('category') != 0 and $request->get('category') !== null){
            $query->where('category_id',intval($request->get('category')));
        }

         if($request->get('source') !== null){
         $query->where('source','LIKE','%'.$request->get('source').'%');
     }

         if($request->get('date') !== null){
             $query->whereDate('publishedAt', '>=', $request->get('date'));
         }
         $data = $query->orderBy('id','DESC')->paginate(15);
         $results = [];
        foreach ($data as $row){
            $results [] = [
                'title'=>$row->title,
                'category'=>@$row->category->title,
                'source'=>@$row->source,
                'date'=>@$row->publishedAt,
            ];
        }

        return response()->json(['status'=>'success','results'=>$results,'lastPage'=>$data->lastPage(),'currentPage'=>$data->currentPage()]);
     }
}

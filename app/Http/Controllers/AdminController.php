<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $count = Article::count();
        $date = Article::orderBy('id','DESC')->first();
        return view('admin',compact('count','date'));
    }

    public function runCommands()
    {

        $oldCount = Article::count();
        Artisan::call('fetch:news');
        Artisan::call('fetch:articles');
        $newCount = Article::count();
        $update = $newCount - $oldCount;
        if($update > 0){
            return redirect()->back()->with('update',$update)->with('status','Commands executed successfully.!');
        }else{
            return redirect()->back()->with('update',$update)->with('status','No new articles have been added. Please try again later!');
        }

    }
}

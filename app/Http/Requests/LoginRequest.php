<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'mobile' => 'required',
            'password' => 'required|min:6',
        ];
    }

    public function messages()
    {
        return [
            'mobile.required' => 'شماره همراه الزامیست!',
            'mobile.regex' => 'شماره همراه ۱۱ رقمی و بصورت 09xxxxxxxxx است!',
            'password.required' => 'کلمه عبور الزامیست',
            'password.min' => 'کلمه عبور حداقل ۶ کاراکتر میباشد',
        ];
    }
}

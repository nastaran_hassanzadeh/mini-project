<?php

namespace App\Console\Commands;

use App\Models\Article;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class FetchArticles extends Command
{

    protected $signature = 'fetch:articles';
    protected $description = 'Fetch articles from the Guardian and store in the database(table:articles)';

    public function handle()
    {
        $apiKey = 'e4eee30f-1996-4fdb-b6ad-fc8cd114a6c4';
        $url = "https://content.guardianapis.com/search?api-key={$apiKey}";

        $result = $this->fetchDataFromApi($url);

        if ($result && $result->response->status === 'ok') {
            foreach ($result->response->results as $article) {
                $this->processArticle($article);
            }
            $this->info('Articles fetched from Guardian and stored successfully.');
        } else {
            $this->error('Failed to fetch articles. API response: ' . $result->response->status);
        }

    }

    protected function fetchDataFromApi($url)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'User-Agent: Your-App-Name/1.0',
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return json_decode($response);
    }


    protected function processArticle($article)
    {
        $category = $this->getOrCreateCategory($article->pillarName);
        $find = Article::where('category_id', $category->id)->where('title', $article->webTitle)->first();

        if (!$find) {
            Article::create([
                'title' => $article->webTitle ?: 'Untitled',
                'source' => $this->getSiteName($article->webUrl),
                'publishedAt' => Carbon::createFromFormat('Y-m-d\TH:i:s\Z', $article->webPublicationDate)->toDateTimeString(),
                'category_id' => $category->id ?: 1,
            ]);
        }
    }

    protected function getOrCreateCategory($categoryTitle)
    {
        $category = Category::whereTitle($categoryTitle)->first();
        if(!$category){
            $category = Category::create(['title'=>$categoryTitle]);
        }

        return $category;
    }


    function getSiteName($url)
    {
        $parsedUrl = parse_url($url);
        if (isset($parsedUrl['host'])) {
            return $parsedUrl['host'];
        }
        return 'Unknown Source';
    }
}

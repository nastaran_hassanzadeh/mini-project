<?php

namespace App\Console\Commands;

use App\Models\Article;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class FetchNews extends Command
{
//    protected $signature = 'app:fetch-articles';
    protected $signature = 'fetch:news';
    protected $description = 'Fetch news from the newsAPI and store in the database(table:articles)';

    public function handle()
    {
        $apiKey = '922b99140fde48619584e8937a15d422';
        $url = "https://newsapi.org/v2/everything?q=keyword&apiKey={$apiKey}";

        $result = $this->fetchDataFromApi($url);

        if ($result && $result->status === 'ok') {
            $this->processArticles($result->articles);
            $this->info('Articles fetched from newsApi and stored successfully.');
        } else {
            $this->error('Failed to fetch articles. API response: ' . $result->status);
        }
    }


    protected function fetchDataFromApi($url)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'User-Agent: Your-App-Name/1.0',
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        return json_decode($response);
    }
    protected function processArticles($articles)
    {
        $category = Category::whereTitle('News')->first();
        if(!$category){
            Category::create([
                'title'=>'News'
            ]);
        }
        foreach ($articles as $article) {
            $this->processArticle($article, $category);
        }
    }

    protected function processArticle($article, $category)
    {
        $find = Article::where('category_id', $category->id)->where('title', $article->title)->first();
        if (!$find) {
            Article::create([
                'title' => $article->title,
                'source' => $article->source->name,
                'description' => $article->description,
                'publishedAt' => Carbon::createFromFormat('Y-m-d\TH:i:s\Z', $article->publishedAt)->toDateTimeString(),
                'category_id' => $category->id ?: 1,
            ]);
        }
    }

}

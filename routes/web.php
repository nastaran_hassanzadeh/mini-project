<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SiteController;

Route::get('/', [SiteController::class, 'index']);

//Route::middleware('auth')->prefix('admin')->name('admin.')->group(function () {
//    Route::controller(AdminController::class)->group(function () {
//        Route::get('/', 'index')->name('list');
//});
//});




Route::get('test-newsApi',function (){
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://newsapi.org/v2/everything?q=keyword&apiKey=922b99140fde48619584e8937a15d422',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    echo $response;

    //    if ($response === false) {
//        echo 'Curl error: ' . curl_error($curl);
//    } else {
//        dd($response);
//    }

});

Auth::routes();

Route::get('/admin', [App\Http\Controllers\AdminController::class, 'index'])->name('admin');
Route::post('/admin/runCommands', [App\Http\Controllers\AdminController::class, 'runCommands'])->name('runCommands');

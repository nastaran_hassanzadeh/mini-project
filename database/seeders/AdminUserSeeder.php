<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Check if admin user already exists
        if (!User::where('email', 'info@admin.com')->exists()) {
            // Create an admin user
            User::create([
                'name' => 'admin',
                'email' => 'info@admin.com',
                'password' => Hash::make('123456'),
                'admin' => 1,
            ]);
        }
    }
}

<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios@1.6.5/dist/axios.min.js" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    var App = new Vue({
        el: '#filterVue',
        data: {
            messages: 'hi vue for filter',
            filterLoading: false,
            results: [],
            stopCall: false,
            page: 1,
            lastPage: null,
            category: '0',
            source:'',
            date:''
        },
        methods: {
            async filter() {
                if (!this.category) {
                    console.error('Category is not selected.');
                    return;
                }

                this.stopCall = false;
                this.page = 1;
                this.results = [];
                this.lastPage = null;
                await this.fetchData();
            },
            async fetchData() {
                if (this.page === 1 && !this.stopCall) {
                    this.filterLoading = true;

                    try {
                        const response = await axios.post(`{{ route('filter-vue') }}`, { category: this.category, page: this.page , 'source':this.source , 'date': this.date});

                        if (response.data.status === 'success') {
                            this.results = response.data.results;
                            this.lastPage = response.data.lastPage;

                            if (this.page < this.lastPage) {
                                setTimeout(async () => {
                                    await this.fetchMoreData();
                                }, 500);
                            } else {
                                this.filterLoading = false;
                                this.stopCall = true;
                            }
                        }
                    } catch (error) {
                        console.error(error);
                    }
                }
            },
            async fetchMoreData() {
                if (this.page < this.lastPage && !this.stopCall) {
                    this.page++;

                    try {
                        const response = await axios.post(`{{ route('filter-vue') }}`, { category: this.category, page: this.page , 'source':this.source , 'date': this.date});

                        if (response.data.results.length > 0) {
                            this.results = [...this.results, ...response.data.results];
                        }

                        setTimeout(async () => {
                            await this.fetchMoreData();
                        }, 500);
                    } catch (error) {
                        console.error(error);
                    }
                } else {
                    this.filterLoading = false;
                    this.stopCall = true;
                }
            },
        },
        watch: {
            category: 'filter',
        },
        mounted() {
            const vm = this;
            $(".date").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            }).on('changeDate', function (e) {
                const selectedDate = moment(e.date).format('YYYY-MM-DD');
                Vue.set(vm, 'date', selectedDate);
                setTimeout(function () {
                    vm.filter();
                }, 0);
            });
            this.filter();
        },
    });
</script>



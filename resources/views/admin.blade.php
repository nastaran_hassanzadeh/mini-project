@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        @if(isset($date))
                        <p>The date of the last update : {{ $date->updated_at }}</p>
                        <p>The date of the last update - Asia/Tehran : {{ jdate('Y/m/d H:i' , $date->updated_at->timestamp) }}</p>
                        @endif
                        @if(isset($count))
                            <p>Total : {{ $count}}</p>
                        @endif
                        <form action="{{ route('runCommands') }}" method="post">
                            @csrf
                            <button type="submit" class="btn btn-primary">Update manually</button>
                        </form>
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection

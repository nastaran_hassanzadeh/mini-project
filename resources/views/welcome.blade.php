<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
    <!-- datepicker  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

    <!-- Custom Styles -->
    <style>
        body {
            font-family: 'figtree', sans-serif;
        }
        .sidebar {
            height: 100%;
            width: 250px;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: #111;
            padding-top: 20px;
            color: white;
        }
        .content {
            margin-left: 250px;
            padding: 20px;
        }
    </style>
</head>
<body class="antialiased">

<div id="filterVue">

    <div class="sidebar"style="padding-right: 20px">
        <h2>Filter</h2>
        <!-- Category Filter -->
        <select class="form-control" v-model="category" @change="filter()">
            @foreach($categories as $id => $title)
                <option value="{{ $id }}">{{ $title }}</option>
            @endforeach
        </select>
       <input class="form-control" placeholder="Type the source..." v-model="source" style="margin-top: 5px" @input="filter()">
        <input class="date form-control" type="text" v-model="date" placeholder="From this date onwards..." style="margin-top: 5px"  @input="filter()">

    </div>

    <div class="content">
        <h2>Items</h2>
        <!-- Display Filtered Items -->
        <div class="row" id="filteredItems">
             <div class="col-md-4 mb-3" v-for="result in results">
                 <div class="card">
                     <div class="card-body">
                         <h5 class="card-title">Title : @{{ result.title }}</h5>
                         <p class="card-text">Category : @{{ result.category }}</p>
                         <p class="card-text">Source : @{{ result.source }}</p>
                         <p class="card-text">Published at : @{{ result.date }}</p>
                     </div>
                 </div>
             </div>
        </div>
    </div>
</div>
@include('vue')
<!-- Bootstrap JS (Optional, for dropdowns, etc.) -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>

<!-- Custom Script -->
<script>

</script>
</body>
</html>
